package com.sc.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sc.app.AppContext;
import com.sc.db.MarkPoint;
import com.sc.lane.R;
import com.sc.ui.SelectPicPopupWindow;

public class PointsAdapter extends BaseAdapter{
	private List<MarkPoint> mPoints;
	private LayoutInflater mInflater;
	private Activity mActivity;
	
	public PointsAdapter(Activity activity, List<MarkPoint> points){
		this.mPoints = points;
		this.mActivity = activity;
		this.mInflater = LayoutInflater.from(mActivity);
	}

	@Override
	public int getCount() {
		if(mPoints == null){
			return 0;
		}
		
		return mPoints.size();
	}

	@Override
	public Object getItem(int position) {
		if(mPoints == null){
			return null;
		}
		
		return mPoints.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		
		holder = new ViewHolder();
		convertView = mInflater.inflate(com.sc.lane.R.layout.view_record_points_item, null);
		
		holder.no = (TextView) convertView.findViewById(R.id.tv_no);
		holder.latitude = (TextView) convertView.findViewById(R.id.tv_latitude);
		holder.longitude = (TextView) convertView.findViewById(R.id.tv_longitude);
		holder.description = (TextView) convertView.findViewById(R.id.tv_description);
		holder.time = (TextView) convertView.findViewById(R.id.tv_time);
		holder.photos = (TextView) convertView.findViewById(R.id.tv_photos);
		holder.attachPhoto = (TextView) convertView.findViewById(R.id.tv_attach_photo);
		
		/* 不能调用 this.getItem方法 */
		final MarkPoint point = mPoints.get(position);
		holder.no.setText(String.valueOf(point.getId()));
		holder.latitude.setText(String.valueOf(point.getLatitude()));
		holder.longitude.setText(String.valueOf(point.getLongitude()));
		holder.description.setText(point.getDescription());
		holder.time.setText(point.getTime());
		holder.photos.setText(point.getPhotos());
		
		holder.attachPhoto.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				mActivity.startActivityForResult(new Intent(mActivity,
						SelectPicPopupWindow.class), 1);
				((AppContext)mActivity.getApplicationContext()).setPhotoAttachedPointId(point.getId());
			}
		});
	
		return convertView;
	}

	public final class ViewHolder {
		public TextView no;
		public TextView latitude;
		public TextView time;
		public TextView longitude;
		public TextView description;
		public TextView photos;
		public TextView attachPhoto;
	}
}
